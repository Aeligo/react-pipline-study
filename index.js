const handler = require('serve-handler');
const http = require('http');
const ngrok = require('ngrok');
const env = require('./src/env/.env.json');

// Part 1 - serve static content at port 5000
const server = http.createServer((request, response) => {
  return handler(request, response, {
      public: "build"
  });
})
 
server.listen(env.port, () => {
    console.log('Running at http://localhost:' + env.port);
});

// Part 2 - use ngrok to expose port 5000 to internet
async function start() {
    const url = await ngrok.connect(env.port);
    console.log("Ngrok tunnel to port " + env.port + "Site name is: " + env.siteName + " exposed at: " + url);
}

start();